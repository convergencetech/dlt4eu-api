# DLT4EU Argan Oil

This repo is an early build of an exciting traceability project, born out of DLT4EU (https://www.dlt4.eu/), that aims to create a credentials-based traceability platform for sustainable commodities. The code base is in a very preliminary state as we finalize some of our design ideas around the tech and will evolve drastically with time as we build out some of these ideas. Feel free to drop us a line if you'd like to collaborate as we get this off the ground and see what this can become!

DLT4EU Argan Oil server api

---
## Requirements

For development, you will only need Node.js and a node global package, NPM, installed in your environement.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v10.19.0

    $ npm --version
    6.14.4

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

---

## Configure app

Creeate `.env` from `.env.example` then edit it with your settings. You will need:

- Web3 provider
- Account private key from where the transactions will be relayed
- Argan Oil contract address

## Running the project

    $ npm install

    $ npm start
