import express from 'express';
import { mintOil, sellOil, claimOil } from '../controllers';

const apiRouter = express.Router();

apiRouter.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

apiRouter.post('/mintOil', mintOil);
apiRouter.post('/sellOil', sellOil);
apiRouter.post('/claimOil', claimOil);

export default apiRouter;
