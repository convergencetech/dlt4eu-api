import express from 'express';

const baseRouter = express.Router();

baseRouter.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

baseRouter.get('/', (req, res) => {
  res.status(200);
  res.send('DOES NOT ACCEPT GET REQUESTS');
});

// ACME-CHALLANGE
baseRouter.get('/.well-known/acme-challenge/e6QlSDema2Gi7evfXP43YXhg7xJhNvgulRJ77cIQ5V0', (req, res) => {
  res.status(200);
  res.send('e6QlSDema2Gi7evfXP43YXhg7xJhNvgulRJ77cIQ5V0.71QVLjFnc4yb1wA3W8j0s7Dv4Uj8GksL13HDt4GowP4');
});

export default baseRouter;
