/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-console */
import Web3 from 'web3';
import logger from 'morgan';
import express from 'express';
import cookieParser from 'cookie-parser';
import apiRouter from './routes/api';
import baseRouter from './routes/extra';
import {
  web3Provider,
  accountPrivateKey,
  arganOilContractAdress,
} from './settings';

const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/v1', apiRouter);
app.use('/', baseRouter);

global.tx = require('ethereumjs-tx').Transaction;
const arganOilAbi = require('../contracts/ArganOilAbi.json');

async function setUp() {
  global.web3 = await new Web3(new Web3.providers.HttpProvider(web3Provider));
  global.arganOilContract = new global.web3.eth.Contract(
    arganOilAbi,
    arganOilContractAdress
  );
  global.gasPrice = 1000;
  global.gasLimit = 3000000;
  global.account = global.web3.eth.accounts.privateKeyToAccount(
    accountPrivateKey
  );
  const balance = await global.web3.eth.getBalance(global.account.address);
  console.log(`Using account: ${global.account.address} --- ${balance / 1e18} eth`);
  console.log(`Using Argan Oil Contract at: ${arganOilContractAdress}`);
  const supply = await global.arganOilContract.methods
    .totalSupply()
    .call({ from: global.account.address });
  console.log(`Total oil minted: ${supply}`);
}

setUp();

export default app;
