/* eslint-disable no-console */
export const sellOil = async (req, res) => {
  console.log('SELLING OIL NFT');
  const data = req.body.packet;
  console.log(data);
  await global.web3.eth.getGasPrice().then((result) => {
    global.gasPrice = result / 1e9;
  });
  console.log(`GAS PRICE: ${global.gasPrice}`);
  const result = await global.arganOilContract.methods
    .delegatedSale(
      data.accountAddress,
      data.id,
      data.verifyHash,
      data.signature
    )
    .send({ from: global.account.address, gas: 3000000, gasPrice: global.gasPrice });
  console.log(result);
  res.status(200).json({
    result,
  });
};
