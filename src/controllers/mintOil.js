/* eslint-disable no-console */
export const mintOil = async (req, res) => {
  console.log('MINTING OIL NFT');
  const data = req.body.packet;
  await global.web3.eth.getGasPrice().then((result) => {
    global.gasPrice = result / 1e9;
  });
  console.log(`GAS PRICE: ${global.gasPrice}`);
  const result = await global.arganOilContract.methods
    .delegatedMint(
      data.accountAddress,
      data.volume,
      data.oilType,
      data.price,
      data.credentials,
      data.nonce,
      data.signedMessage
    )
    .send({ from: global.account.address, gas: 3000000, gasPrice: global.gasPrice });
  console.log(result);
  res.status(200).json({
    result,
  });
};
