import dotenv from 'dotenv';

dotenv.config();
export const port = process.env.PORT;
export const web3Provider = process.env.WEB3_PROVIDER;
export const accountPrivateKey = process.env.ACCOUNT_PRIVATE_KEY;
export const arganOilContractAdress = process.env.ARGAN_OIL_CONTRACT_ADDRESS;
