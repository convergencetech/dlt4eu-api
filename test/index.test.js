import { expect, server, BASE_URL } from './setup';

describe('Index page test', () => {
  it('gets base url', (done) => {
    server
      .get(`${BASE_URL}/`)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });
});

describe('Send Transaction test', () => {
  it('submits transaction', (done) => {
    server
      .post(`${BASE_URL}/sendTransaction`)
      .send({
        accountAddress: '0x30B7b3a9c2ef3Bb4003d3D1a8BEB39D042155e9F',
        cridentials:
          '0x290decd9548b62a8d60345a988386fc84ba6bc95484008f6362f93160ef3e563',
        nonce:
          '0x39e7432b823ec57683eb6c87a33d4aa71f638254d668ca46c6b13046801d7f52',
        signedMessage:
          '0xccb1cff82a64ec1202ff80b66438f71ac6c0efe8b197cc60067f701a017137c21d9b495ecde932daf43c301861b6aaf9e8d4ba919422ee2338e37518deb515191b',
        volume: '123',
        weight: '123'
      })
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  }).timeout(5000);
});
