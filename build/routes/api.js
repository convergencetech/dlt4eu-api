"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _controllers = require("../controllers");

var apiRouter = _express["default"].Router();

apiRouter.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
apiRouter.post('/mintOil', _controllers.mintOil);
apiRouter.post('/sellOil', _controllers.sellOil);
apiRouter.post('/claimOil', _controllers.claimOil);
var _default = apiRouter;
exports["default"] = _default;