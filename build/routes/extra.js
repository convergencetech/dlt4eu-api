"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var baseRouter = _express["default"].Router();

baseRouter.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
baseRouter.get('/', function (req, res) {
  res.status(200);
  res.send('DOES NOT ACCEPT GET REQUESTS');
}); // ACME-CHALLANGE

baseRouter.get('/.well-known/acme-challenge/e6QlSDema2Gi7evfXP43YXhg7xJhNvgulRJ77cIQ5V0', function (req, res) {
  res.status(200);
  res.send('e6QlSDema2Gi7evfXP43YXhg7xJhNvgulRJ77cIQ5V0.71QVLjFnc4yb1wA3W8j0s7Dv4Uj8GksL13HDt4GowP4');
});
var _default = baseRouter;
exports["default"] = _default;