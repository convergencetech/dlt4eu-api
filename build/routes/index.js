"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _controllers = require("../controllers");

var indexRouter = _express["default"].Router();

indexRouter.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
indexRouter.get('/', _controllers.indexPage);
indexRouter.post('/sendTransaction', _controllers.sendTransaction);
var _default = indexRouter;
exports["default"] = _default;