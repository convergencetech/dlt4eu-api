"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _web = _interopRequireDefault(require("web3"));

var _morgan = _interopRequireDefault(require("morgan"));

var _express = _interopRequireDefault(require("express"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _api = _interopRequireDefault(require("./routes/api"));

var _extra = _interopRequireDefault(require("./routes/extra"));

var _settings = require("./settings");

/* eslint-disable prefer-arrow-callback */

/* eslint-disable no-console */
var app = (0, _express["default"])();
app.use((0, _morgan["default"])('dev'));
app.use(_express["default"].json());
app.use(_express["default"].urlencoded({
  extended: true
}));
app.use((0, _cookieParser["default"])());
app.use('/v1', _api["default"]);
app.use('/', _extra["default"]);
global.tx = require('ethereumjs-tx').Transaction;

var arganOilAbi = require('../contracts/ArganOilAbi.json');

function setUp() {
  return _setUp.apply(this, arguments);
}

function _setUp() {
  _setUp = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    var balance, supply;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return new _web["default"](new _web["default"].providers.HttpProvider(_settings.web3Provider));

          case 2:
            global.web3 = _context.sent;
            global.arganOilContract = new global.web3.eth.Contract(arganOilAbi, _settings.arganOilContractAdress);
            global.gasPrice = 1000;
            global.gasLimit = 3000000;
            global.account = global.web3.eth.accounts.privateKeyToAccount(_settings.accountPrivateKey);
            _context.next = 9;
            return global.web3.eth.getBalance(global.account.address);

          case 9:
            balance = _context.sent;
            console.log("Using account: ".concat(global.account.address, " --- ").concat(balance / 1e18, " eth"));
            console.log("Using Argan Oil Contract at: ".concat(_settings.arganOilContractAdress));
            _context.next = 14;
            return global.arganOilContract.methods.totalSupply().call({
              from: global.account.address
            });

          case 14:
            supply = _context.sent;
            console.log("Total oil minted: ".concat(supply));

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _setUp.apply(this, arguments);
}

setUp();
var _default = app;
exports["default"] = _default;