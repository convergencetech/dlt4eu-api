"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arganOilContractAdress = exports.accountPrivateKey = exports.web3Provider = exports.port = void 0;

var _dotenv = _interopRequireDefault(require("dotenv"));

_dotenv["default"].config();

var port = process.env.PORT;
exports.port = port;
var web3Provider = process.env.WEB3_PROVIDER;
exports.web3Provider = web3Provider;
var accountPrivateKey = process.env.ACCOUNT_PRIVATE_KEY;
exports.accountPrivateKey = accountPrivateKey;
var arganOilContractAdress = process.env.ARGAN_OIL_CONTRACT_ADDRESS;
exports.arganOilContractAdress = arganOilContractAdress;