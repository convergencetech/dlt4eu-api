"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mintOil = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

/* eslint-disable no-console */
var mintOil = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var data, result;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log('MINTING OIL NFT');
            data = req.body.packet;
            _context.next = 4;
            return global.web3.eth.getGasPrice().then(function (result) {
              global.gasPrice = result / 1e9;
            });

          case 4:
            console.log("GAS PRICE: ".concat(global.gasPrice));
            _context.next = 7;
            return global.arganOilContract.methods.delegatedMint(data.accountAddress, data.volume, data.oilType, data.price, data.credentials, data.nonce, data.signedMessage).send({
              from: global.account.address,
              gas: 3000000,
              gasPrice: global.gasPrice
            });

          case 7:
            result = _context.sent;
            console.log(result);
            res.status(200).json({
              result: result
            });

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function mintOil(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.mintOil = mintOil;