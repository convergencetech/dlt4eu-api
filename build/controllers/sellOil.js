"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sellOil = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

/* eslint-disable no-console */
var sellOil = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var data, result;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log('SELLING OIL NFT');
            data = req.body.packet;
            console.log(data);
            _context.next = 5;
            return global.web3.eth.getGasPrice().then(function (result) {
              global.gasPrice = result / 1e9;
            });

          case 5:
            console.log("GAS PRICE: ".concat(global.gasPrice));
            _context.next = 8;
            return global.arganOilContract.methods.delegatedSale(data.accountAddress, data.id, data.verifyHash, data.signature).send({
              from: global.account.address,
              gas: 3000000,
              gasPrice: global.gasPrice
            });

          case 8:
            result = _context.sent;
            console.log(result);
            res.status(200).json({
              result: result
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function sellOil(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.sellOil = sellOil;